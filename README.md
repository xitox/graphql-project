# graphql-project

## Project presentation

I didn't use the exact same method to create my database as I started from the prisma.schemas to migrate it into my database.
I used appolo-server in addition to express because I saw a lot of online tutoriel using it.
I have a preference for typescript (even if it wasn't easy to use it with prisma).

My project is located in src and the entrypoint is index.ts.
I wanted to go further than the class by trying to create a project that could be professional.
To do so, I separated the schemas and resolvers in differents file. It could be argued that the schema should be in separated files and I think it would be true for a bigger database.
I also tried to prevent code repetition and improve maintanance efficiency by creating generic functions for all CRUD opertations except update.

## How to launch

### Rquirements

You need to have the followings if you want to start the application

- node
- postgres database

**2 options for the database:**
- Use your own. If you choose this option you need to make sure your `.env` is configured accordingly to your own database. Default being, user = graphql, password = graphql, db = graphql_project, server = localhost, port = 5432.
- Use docker-compose with the file I provided. **Note that for docker everything is preconfigured**

### Launching instruction
1. Go to project root
2. run `npm install`
3. Start your database
>If you are using the premade docker database just run `docker-compose up -d` <br />
>If you are using your own, make sure you configure your .env file <br />
4. migrate the schemas into your database with `npx prisma migrate dev --name init`
5. Finally, start the application by running `npm run start:watch`
**Note that this script is made so the application is watching for filechange and restart accordingly**

### Test some request

I made a postman workspace and I made it available for the adress mail you gave us.
`melvin.bisson@andn-services.fr`
If you want me to share it to another mail adress please contact me on `nicolas.pernel@outlook.fr`