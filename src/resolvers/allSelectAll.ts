import { genericSelectAll } from "./generic";

export const selectAllQueries = {
    async allParcours(_parent: any, _args: any, context: any) {
        return await genericSelectAll("parcours", { matieres: true }, context);
    },

    async allMatieres(_parent: any, _args: any, context: any) {
        return await genericSelectAll("matiere", { parcours: true }, context);
    },

    async allCours(_parent: any, _args: any, context: any) {
        return await genericSelectAll("cours", { matiere: true }, context);
    },

    async allClasses(_parent: any, _args: any, context: any) {
        return await genericSelectAll("classe", { eleves: true }, context);
    },

    async allEleves(_parent: any, _args: any, context: any) {
        return await genericSelectAll("eleve", { classe: true }, context);
    },

    async allNotes(_parent: any, _args: any, context: any) {
        return await genericSelectAll("note", { eleve: true, cours: true }, context);
    },

    async allFormateurs(_parent: any, _args: any, context: any) {
        return await genericSelectAll("formateur", { matieres: { include: { matiere: true } } }, context);
    },

    async allMatiereFormateurs(_parent: any, _args: any, context: any) {
        return await genericSelectAll("matiereFormateur", { formateur: true, matiere: true }, context);
    },
};      