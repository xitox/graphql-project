import { genericDelete } from "./generic";

export const deleteMutations = {
    async deleteFormateur(_parent: any, { id }: any, context: any) {
        return await genericDelete('formateur', { where: { id } }, context);
    },

    async deleteParcours(_parent: any, { id }: any, context: any) {
        return await genericDelete('parcours', { where: { id } }, context);
    },

    async deleteMatiere(_parent: any, { id }: any, context: any) {
        return await genericDelete('matiere', { where: { id } }, context);
    },

    async deleteClasse(_parent: any, { id }: any, context: any) {
        return await genericDelete('classe', { where: { id } }, context);
    },

    async deleteEleve(_parent: any, { id }: any, context: any) {
        return await genericDelete('eleve', { where: { id } }, context);
    },

    async deleteNote(_parent: any, { id }: any, context: any) {
        return await genericDelete('note', { where: { id } }, context);
    },

    async deleteMatiereFormateur(_parent: any, { id }: any, context: any) {
        return await genericDelete('matiereFormateur', { where: { id } }, context);
    },
};
