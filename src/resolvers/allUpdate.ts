// src/resolvers/updateMutations.ts
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const updateMutations = {
    async updateParcours(parent: any, args: any, context: any) {
        return await prisma.parcours.update({
            where: { id: args.id },
            data: { nom: args.nom },
        });
    },
    async updateMatiere(parent: any, args: any, context: any) {
        return await prisma.matiere.update({
            where: { id: args.id },
            data: {
                nom: args.nom,
                parcoursId: args.parcoursId,
            },
        });
    },
    async updateCours(parent: any, args: any, context: any) {
        return await prisma.cours.update({
            where: { id: args.id },
            data: {
                nom: args.nom,
                matiereId: args.matiereId,
            },
        });
    },
    async updateClasse(parent: any, args: any, context: any) {
        return await prisma.classe.update({
            where: { id: args.id },
            data: { nom: args.nom },
        });
    },
    async updateEleve(parent: any, args: any, context: any) {
        return await prisma.eleve.update({
            where: { id: args.id },
            data: {
                nom: args.nom,
                prenom: args.prenom,
                classeId: args.classeId,
            },
        });
    },
    async updateNote(parent: any, args: any, context: any) {
        return await prisma.note.update({
            where: { id: args.id },
            data: {
                valeur: args.valeur,
                eleveId: args.eleveId,
                coursId: args.coursId,
            },
        });
    },
    async updateFormateur(parent: any, args: any, context: any) {
        return await prisma.formateur.update({
            where: { id: args.id },
            data: {
                nom: args.nom,
                prenom: args.prenom,
            },
        });
    },
    async updateMatiereFormateur(parent: any, args: any, context: any) {
        return await prisma.matiereFormateur.update({
            where: { id: args.id },
            data: {
                formateurId: args.formateurId,
                matiereId: args.matiereId,
            },
        });
    },
};
