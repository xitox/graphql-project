import { genericCreate } from "./generic";

export const createMutations = {
    async createParcours(
        _parent: any,
        { nom }: { nom: string },
        context: any,
    ) {
        return await genericCreate("parcours", { nom }, null, context);
    },
    async createMatiere(
        _parent: any,
        { nom, parcoursId }: { nom: string; parcoursId: string },
        context: any
    ) {
        return await genericCreate(
            "matiere",
            {
                nom,
                parcours: {
                    connect: {
                        id: parcoursId,
                    },
                },
            },
            { parcours: true },
            context
        );
    },
    async createCours(
        _parent: any,
        { nom, matiereId }: { nom: string; matiereId: string },
        context: any
    ) {
        return await genericCreate(
            "cours",
            {
                nom,
                matiere: {
                    connect: {
                        id: matiereId,
                    },
                },
            },
            { matiere: true },
            context
        );
    },

    async createClasse(
        _parent: any,
        { nom }: { nom: string },
        context: any
    ) {
        return await genericCreate("classe", { nom }, null, context);
    },

    async createEleve(
        _parent: any,
        { nom, prenom, classeId }: { nom: string; prenom: string; classeId: string },
        context: any
    ) {
        return await genericCreate(
            "eleve",
            {
                nom,
                prenom,
                classe: {
                    connect: {
                        id: classeId,
                    },
                },
            },
            { classe: true },
            context
        );
    },

    async createNote(
        _parent: any,
        {
            valeur,
            eleveId,
            coursId,
        }: { valeur: number; eleveId: string; coursId: string },
        context: any
    ) {
        return await genericCreate(
            "note",
            {
                valeur,
                eleve: {
                    connect: {
                        id: eleveId,
                    },
                },
                cours: {
                    connect: {
                        id: coursId,
                    },
                },
            },
            { eleve: true, cours: true },
            context
        );
    },

    async createFormateur(
        _parent: any,
        { nom, prenom }: { nom: string; prenom: string },
        context: any
    ) {
        return await genericCreate("formateur", { nom, prenom }, null, context);
    },

    async createMatiereFormateur(
        _parent: any,
        { formateurId, matiereId }: { formateurId: string; matiereId: string },
        context: any
    ) {
        return await genericCreate(
            "matiereFormateur",
            {
                formateur: {
                    connect: {
                        id: formateurId,
                    },
                },
                matiere: {
                    connect: {
                        id: matiereId,
                    },
                },
            },
            { formateur: true, matiere: true },
            context
        );
    },
}