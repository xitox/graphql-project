export async function genericCreate(
    model: string,
    data: any,
    include: any,
    { prisma }: any
) {
    return await prisma[model].create({
        data: data,
        include: include,
    });
}

export async function genericSelectAll(
    model: string,
    include: any,
    { prisma }: any
) {
    const modelName = model.charAt(0).toUpperCase() + model.slice(1);
    return await prisma[modelName].findMany({
        include: include,
    });
}

export const genericSelectById = async (
    model: string,
    options: any,
    { prisma }: any
) => {
    return await prisma[model].findUnique(options);
};

export const genericDelete = async (
    model: string,
    options: any,
    { prisma }: any
) => {
    return await prisma[model].delete(options);
};