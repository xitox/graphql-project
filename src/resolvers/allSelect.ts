import { genericSelectById } from './generic';

export const selectByIdQueries = {
    async parcours(_parent: any, { id }: any, context: any) {
        return await genericSelectById('parcours', { where: { id } }, context);
    },

    async matiere(_parent: any, { id }: any, context: any) {
        return await genericSelectById('matiere', { where: { id } }, context);
    },

    async cours(_parent: any, { id }: any, context: any) {
        return await genericSelectById('cours', { where: { id } }, context);
    },

    async classe(_parent: any, { id }: any, context: any) {
        return await genericSelectById('classe', { where: { id } }, context);
    },

    async eleve(_parent: any, { id }: any, context: any) {
        return await genericSelectById('eleve', { where: { id } }, context);
    },

    async note(_parent: any, { id }: any, context: any) {
        return await genericSelectById('note', { where: { id } }, context);
    },

    async formateur(_parent: any, { id }: any, context: any) {
        return await genericSelectById('formateur', { where: { id } }, context);
    },

    async matiereFormateur(_parent: any, { id }: any, context: any) {
        return await genericSelectById('matiereFormateur', { where: { id } }, context);
    },
}
