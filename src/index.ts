import express from "express";
import { ApolloServer, gql } from "apollo-server-express";
import { PrismaClient } from "@prisma/client";
import { typeDefs } from "./typeDefs";
import { createMutations } from "./resolvers/allCreates";
import { selectAllQueries } from "./resolvers/allSelectAll";
import { selectByIdQueries } from "./resolvers/allSelect";
import { deleteMutations } from "./resolvers/allDelete";
import { updateMutations } from "./resolvers/allUpdate";

const prisma = new PrismaClient();

const combinedQueries = {
    ...selectAllQueries,
    ...selectByIdQueries
};

const combinedMutations = {
    ...createMutations,
    ...deleteMutations,
    ...updateMutations
};

const resolvers = {
    Query: combinedQueries,
    Mutation: combinedMutations,
};

(async () => {
    const server = new ApolloServer({
        typeDefs,
        resolvers,
        context: ({ req }) => {
            return {
                ...req,
                prisma,
            };
        },
    });
    await server.start();

    const app = express();
    server.applyMiddleware({ app });

    app.listen({ port: 4000 }, () =>
        console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
    );
})();