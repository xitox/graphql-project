import { gql } from "apollo-server-express";

export const typeDefs = gql`
  type Parcours {
    id: Int!
    nom: String!
    matieres: [Matiere!]!
  }

  type Matiere {
    id: Int!
    nom: String!
    parcoursId: Int!
    parcours: Parcours!
    cours: [Cours!]!
    formateurs: [Formateur!]!
  }

  type Cours {
    id: Int!
    nom: String!
    matiereId: Int!
    matiere: Matiere!
    notes: [Note!]!
  }

  type Classe {
    id: Int!
    nom: String!
    eleves: [Eleve!]!
  }

  type Eleve {
    id: Int!
    nom: String!
    prenom: String!
    classeId: Int!
    classe: Classe!
    notes: [Note!]!
  }

  type Note {
    id: Int!
    valeur: Float!
    eleveId: Int!
    eleve: Eleve!
    coursId: Int!
    cours: Cours!
  }

  type Formateur {
    id: Int!
    nom: String!
    prenom: String!
    matieres: [MatiereFormateur!]!
  }

  type MatiereFormateur {
    id: Int!
    formateurId: Int!
    formateur: Formateur!
    matiereId: Int!
    matiere: Matiere!
  }

  type Query {
    allParcours: [Parcours!]!
    parcours(id: Int!): Parcours
    allMatieres: [Matiere!]!
    matiere(id: Int!): Matiere
    allCours: [Cours!]!
    cours(id: Int!): Cours
    allClasses: [Classe!]!
    classe(id: Int!): Classe
    allEleves: [Eleve!]!
    eleve(id: Int!): Eleve
    allNotes: [Note!]!
    note(id: Int!): Note
    allFormateurs: [Formateur!]!
    formateur(id: Int!): Formateur
    allMatiereFormateurs: [MatiereFormateur!]!
    matiereFormateur(id: Int!): MatiereFormateur
  }

  type Mutation {
    createParcours(nom: String!): Parcours!
    updateParcours(id: Int!, nom: String!): Parcours!
    deleteParcours(id: Int!): Parcours!
    createMatiere(nom: String!, parcoursId: Int!): Matiere!
    updateMatiere(id: Int!, nom: String!, parcoursId: Int!): Matiere!
    deleteMatiere(id: Int!): Matiere!
    createCours(nom: String!, matiereId: Int!): Cours!
    updateCours(id: Int!, nom: String!, matiereId: Int!): Cours!
    deleteCours(id: Int!): Cours!
    createClasse(nom: String!): Classe!
    updateClasse(id: Int!, nom: String!): Classe!
    deleteClasse(id: Int!): Classe!
    createEleve(nom: String!, prenom: String!, classeId: Int!): Eleve!
    updateEleve(id: Int!, nom: String!, prenom: String!, classeId: Int!): Eleve!
    deleteEleve(id: Int!): Eleve!
    createNote(valeur: Float!, eleveId: Int!, coursId: Int!): Note!
    updateNote(id: Int!, valeur: Float!, eleveId: Int!, coursId: Int!): Note!
    deleteNote(id: Int!): Note!
    createFormateur(nom: String!, prenom: String!): Formateur!
    updateFormateur(id: Int!, nom: String!, prenom: String!): Formateur!
    deleteFormateur(id: Int!): Formateur!
    createMatiereFormateur(formateurId: Int!, matiereId: Int!): MatiereFormateur!
    updateMatiereFormateur(id: Int!, formateurId: Int!, matiereId: Int!): MatiereFormateur!
    deleteMatiereFormateur(id: Int!): MatiereFormateur!
  }
`;